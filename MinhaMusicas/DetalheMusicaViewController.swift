//
//  DetalheMusicaViewController.swift
//  MinhaMusicas
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

class DetalheMusicaViewController: UIViewController {
    
    var nomeMusica : String = ""
    var nomeAlbum : String = ""
    var nomeCantor : String = ""
    var nomeImagem : String = ""
    

    

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.Imagem.image = UIImage(named: self.nomeImagem)
        self.NomeDoAlbum.text = self.nomeAlbum
        self.NomeDoCantor.text = self.nomeCantor
        self.NomeDaMusica.text = self.nomeMusica
    }
    @IBOutlet weak var Imagem: UIImageView!
    @IBOutlet weak var NomeDaMusica: UILabel!
    @IBOutlet weak var NomeDoCantor: UILabel!
    @IBOutlet weak var NomeDoAlbum: UILabel!
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
